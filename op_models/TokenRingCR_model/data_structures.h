#ifndef STRUCTURES_H
#define STRUCTURES_H
  
struct flow_struct{
	double 	mean_inter_time;
	double 	strt_dly;
	int 	mean_length;
	int		DA;
	int		SA;
	int		traffic_ID;
};

#endif
